package FinalWebTest;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import java.util.NoSuchElementException;

public class LoginTest extends AbstractTest {
    @Test
    @DisplayName("Авторизация с валидными данными")
    @Tag("Positive")
    public void LoginValidTest() {
        getLoginPage().login(getLogin(), getPass());
        try {
            String textAccount = getMainPage().getMenuAcc().getText();
            Assertions.assertEquals(textAccount, "Hello, GB202302d8e3c11");
            getMainPage().getLogoHome();
            getMainPage().getLastPost();
        } catch (NoSuchElementException e){
            Assertions.assertTrue(false);
        }
        Assertions.assertEquals(getDriver().getCurrentUrl(), getBaseUrl());
    }

    @ParameterizedTest
    @CsvFileSource(resources = "invalidTestData.csv")
    @DisplayName("Авторизация с невалидными данными")
    public void LoginInValidTest(String login, String pass) {
        getLoginPage().login(login, pass);

        try{
            String error = getLoginPage().getErrorHeader().getText();
            Assertions.assertEquals(error, "401");
        } catch (NoSuchElementException e){
            Assertions.assertTrue(false);
        }
        Assertions.assertEquals(getDriver().getCurrentUrl(), getLoginUrl());
    }

    @ParameterizedTest
    @CsvFileSource(resources = "validCornerCase.csv")
    @DisplayName("Авторизация. Валидные учетные данные")
    @Tag("Positive")
    public void LoginValidCornerTest(String login, String pass)  {
        getLoginPage().login(login, pass);

        try {
            getMainPage().getLogoHome();
        } catch (NoSuchElementException e) {
            Assertions.assertTrue(false);
        }

        Assertions.assertEquals(getDriver().getCurrentUrl(), getBaseUrl());
    }

    
    @ParameterizedTest
    @CsvFileSource(resources = "invalidCornerCase.csv")
    @DisplayName("Авторизация. Невалидные учетные данные")
    @Tag("Negative")
    public void LoginInValidCornerTest(String login, String pass) {
        getLoginPage().login(login, pass);

        String submitColor = getLoginPage().getLoginSubmit().getCssValue("color");
        Assertions.assertEquals(submitColor, "rgba(255, 255, 255, 1)");

        try {
            getLoginPage().getErrorHeader();
            Assertions.assertTrue(false);
        } catch (NoSuchElementException e) {
            Assertions.assertTrue(true);
        }

        Assertions.assertEquals(getDriver().getCurrentUrl(), getLoginUrl());

    }

}




package FinalWebTest;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

    @Getter
    public class MainPage extends AbstractPage {
        @FindBy(xpath = ".//li[@class=\"svelte-1rc85o5 mdc-menu-surface--anchor\"]/a")
        private WebElement menuAcc;
        @FindBy(xpath = ".//ul[@role=\"menu\"]/li[3]")
        private WebElement logout;

        @FindBy(xpath = ".//a[@href=\"/posts/13988\"]")
        private WebElement lastPost;

        @FindBy(xpath = ".//a[@href=\"/posts/13987\"]")
        private WebElement firstPost;

        @FindBy(xpath = ".//a[@href=\"/posts/14739\"]/img")
        private WebElement firstPostImg;

        @FindBy(xpath = ".//a[@href=\"/posts/14737\"]/h2")
        private WebElement firstPostTitle;

        @FindBy(xpath = ".//a[@href=\"/posts/14737\"]/div")
        private WebElement firstPostDescription;

        @FindBy(xpath = ".//a[@class=\"logo svelte-1rc85o5\"]/span")
        private WebElement logoHome;

        @FindBy(xpath = ".//div[@class=\"content\"]")
        private WebElement noContent;

        @FindBy(xpath = ".//button[@aria-pressed]")
        private WebElement sortButton;

        @FindBy(xpath = ".//div[@class=\"content\"]/div[2]/div/a[1]")
        private WebElement previousPage;

        @FindBy(xpath = ".//div[@class=\"pagination svelte-d01pfs\"]/a[2]")
        private WebElement nextPage;


        public void clickSort() {
            this.sortButton.click();
        }

        public void clickNextPage() {
            this.nextPage.click();
        }

        public void clickPrevPage() {
            this.previousPage.click();
        }

        public void logout() {
            Actions logout = new Actions(getDriver());
            logout
                    .click(this.menuAcc)
                    .pause(2000)
                    .click(this.logout)
                    .build()
                    .perform();
        }

        public MainPage(WebDriver driver) {
            super(driver);
        }
    }




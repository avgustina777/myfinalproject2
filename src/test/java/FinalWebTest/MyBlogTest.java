package FinalWebTest;

import org.junit.jupiter.api.*;
import org.openqa.selenium.NoSuchElementException;

public class MyBlogTest extends AbstractTest {
    @Test
    @DisplayName("Отображение постов  пользователя, у которого есть посты")
    // @Tag("Positive")
    public void postDisplayTest() {
        getLoginPage().login(getLogin(), getPass());
        getMainPage().clickSort();
        getMainPage().clickSort();

        try {
            String firstPostImg = getMainPage().getFirstPostImg().getAttribute("src");
            Assertions.assertEquals(firstPostImg, "http://test-stand.gb.ru/files/public/image/b30140f124b90a7037994b4b70f7c417.png");

            String firstPostTitle = getMainPage().getFirstPostTitle().getText();
            Assertions.assertEquals(firstPostTitle, "Первый пост");

            String firstPostDescr = getMainPage().getFirstPostDescription().getText();
            Assertions.assertEquals(firstPostDescr, "пост");
        } catch (NoSuchElementException e) {
            Assertions.assertTrue(false);
        }


    }

    @Test
    @DisplayName("Отображение пустой картинки")
   // @Tag("Positive")
    public void NoContentPostTest() {
        getLoginPage().login(getEmptyLogin(), getEmptyPass());
        try {
            String textNoContent = getMainPage().getNoContent().getText();
            Assertions.assertEquals(textNoContent, "No items for your filter");
        } catch (NoSuchElementException e) {
            Assertions.assertTrue(false);
        }

    }
    @Test
    @DisplayName("Переход на следующую страницу")
   // @Tag("Positive")
    public void clickNextPagesTest() throws InterruptedException {
        getLoginPage().login(getLogin(), getPass());

        try {
            String nextPageHref = getMainPage().getNextPage().getAttribute("href");
            assert (nextPageHref.contains("/?page=2"));

            String nextPageText = getMainPage().getNextPage().getText();
            Assertions.assertEquals(nextPageText, "Next Page");

            getMainPage().clickNextPage();
            Thread.sleep(5000);
            Assertions.assertEquals(getDriver().getCurrentUrl(), "https://test-stand.gb.ru/api/posts?sort=createdAt&order=ASC&page=2");
            getMainPage().getLastPost();

        } catch (NoSuchElementException e) {
            Assertions.assertTrue(false);
        }
    }

    @Test
    @DisplayName("Переход на предыдущую страницу")
    //@Tag("Positive")
    public void clickPrevPagesTest() throws InterruptedException {

        getLoginPage().login(getLogin(), getPass());

        try {
            String previousPageText = getMainPage().getPreviousPage().getText();
            Assertions.assertEquals(previousPageText, "Previous Page");
            String previousPageClass = getMainPage().getPreviousPage().getAttribute("class");
            assert (previousPageClass.contains("disabled"));

            getMainPage().clickNextPage();
            Thread.sleep(5000);
            String prevPageHref = getMainPage().getPreviousPage().getAttribute("href");
            assert (prevPageHref.contains("/?page=1"));

            getMainPage().clickPrevPage();
            Thread.sleep(5000);
            assert (getDriver().getCurrentUrl().contains("/?page=1"));

        } catch (NoSuchElementException e) {
            Assertions.assertTrue(false);
        }
    }


}


